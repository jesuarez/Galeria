<?php


 ?>

 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0">
         <title>Galeria Fotográfica</title>
         <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
         <link rel="stylesheet" href="css/estilos.css">
     </head>
     <body>
         <header>
             <div class="contenedor">
                 <h1 class="titulo">Foto: 1.jpg</h1>
             </div>
         </header>
         <div class="contenedor">
             <div class="foto">
                 <img src="imagenes/1.jpg" alt="">
                 <p class="texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                 <a href="index.php" class="regresar"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Regresar a la galeria</a>
             </div>
         </div>
         <footer><p class="copyright">Galeria Creada por Jorge Suárez</p></footer>
     </body>
 </html>
