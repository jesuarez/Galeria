<?php


 ?>

 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0">
         <title>Galeria Fotográfica</title>
         <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
         <link rel="stylesheet" href="css/estilos.css">
     </head>
     <body>
         <header>
             <div class="contenedor">
                 <h1 class="titulo">Galeria Fotográfica</h1>
             </div>
         </header>
         <section class="fotos">
             <div class="contenedor">
                 <div class="thumb">
                     <a href="#">
                         <img src="imagenes/1.jpg" alt="">
                     </a>
                 </div>
                 <div class="thumb">
                     <a href="#">
                         <img src="imagenes/2.jpg" alt="">
                     </a>
                 </div>
                 <div class="thumb">
                     <a href="#">
                         <img src="imagenes/3.jpg" alt="">
                     </a>
                 </div>
                 <div class="thumb">
                     <a href="#">
                         <img src="imagenes/4.jpg" alt="">
                     </a>
                 </div>
                 <div class="paginacion">
                     <a href="#" class="izquierda"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Página Anterior</a>
                     <a href="#" class="derecha">Página Siguiente <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                 </div>
             </div>
         </section>
         <footer><p class="copyright">Galeria Creada por Jorge Suárez</p></footer>
     </body>
 </html>
