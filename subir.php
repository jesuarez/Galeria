<?php
    require_once 'funciones/funciones.php';

    $conexion = conexion('galeria', 'root', '123456');
    if (!$conexion) {
        header('Location: error.php');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_FILES)) {
        $check = @getimagesize($_FILES['foto']['tmp_name']); //La funcion getimagesize nos permite conocer si realmente en el arreglo viene una imagen, en caso que no sea una imagen nos devuelve false
        if ($check !== false) {
            $carpeta_destino = 'fotos/';
            $archivo_subido = $carpeta_destino .$_FILES['foto']['name'];
            move_uploaded_file($_FILES['foto']['tmp_name'], $archivo_subido);
            $statement = $conexion->prepare(
                'INSERT INTO fotos (id, titulo, imagen, texto)
                VALUES (NULL, :titulo, :imagen, :texto)');
            
            $statement->execute(array(
                ':titulo' => $_POST['titulo'],
                ':imagen' => $_FILES['foto']['name'],
                ':texto' => $_POST['texto']
            ));
            header('Location: index.php');
        }else {
            $errores = 'El archivo no es una imagen o el archivo es muy pesado';
        }
    }

 ?>

 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0">
         <title>Galeria Fotográfica</title>
         <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
         <link rel="stylesheet" href="css/estilos.css">
     </head>
     <body>
         <header>
             <div class="contenedor">
                 <h1 class="titulo">Subir Foto</h1>
             </div>
         </header>
         <div class="contenedor">
             <form class="formulario" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data"> <!--enctype es obligatorio para poder subir archivos-->
                    <label for="foto">Selecciona tu foto</label>
                    <input type="file" name="foto" id="foto" value="" >
                    <label for="titulo">Titulo de la foto</label>
                    <input type="text" name="titulo" id="titulo" value="" >
                    <label for="texto">Descripción</label>
                    <textarea name="texto" id="texto" placeholder="Ingresa una descripción"></textarea>
                    <input type="submit" class="submit" name="submit" value="Subir Foto">
                    <?php if(isset($errores)): ?>
                        <p class="error"><?= $errores; ?></p>
                    <?php endif; ?>
             </form>
         </div>
         <footer><p class="copyright">Galeria Creada por Jorge Suárez</p></footer>
     </body>
 </html>
